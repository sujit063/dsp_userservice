package com.dsp.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DspUserserviceApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(DspUserserviceApplication.class, args);
		
	}

}
